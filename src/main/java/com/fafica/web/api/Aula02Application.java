package com.fafica.web.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.fafica.web.api.config.property.FaficaAPIProperty;



@SpringBootApplication
@EnableConfigurationProperties(FaficaAPIProperty.class)
public class Aula02Application {

	public static void main(String[] args) {
		SpringApplication.run(Aula02Application.class, args);
	}
}
