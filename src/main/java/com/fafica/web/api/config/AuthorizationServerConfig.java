package com.fafica.web.api.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.fafica.web.api.security.AppUserDetailsService;

@Configuration
@EnableAuthorizationServer
@Profile("oauth-security")
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	@Autowired
	private AuthenticationManager am; // esse que pega do ResourceServer as credenciais

	/*@Autowired
    private AppUserDetailsService userDetailsService;*/
	
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
		//DEFINIÇÃO  DA CONFIGURAÇÃO DOS CLIENTES
		//ESCOPO DE CLIENTES DIFERENTES READ AND WRITE 
		//CLIENTE ANGULAR PODE TUDO 
			.withClient("angular")
			.secret("{noop}@ngular")
			.scopes("read", "write")
			.authorizedGrantTypes("password","refresh_token")
			.accessTokenValiditySeconds(20)
			.refreshTokenValiditySeconds(3600*24)
		.and()// CLIENTE MOBILE SÓ PODE LER
		.withClient("mobile")
		.secret("{noop}mobile")
		.scopes("read")
		.authorizedGrantTypes("password","refresh_token")
		.accessTokenValiditySeconds(20)
		.refreshTokenValiditySeconds(3600*24);
	}

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints
			.tokenStore(tokenStore())
			.accessTokenConverter(accessTokenConverter())
			.reuseRefreshTokens(false)//Enquanto o usuário estiver usando a
			//app todos os dias o refreshtoken não vai expirar, se não setar o prazo de de 24 horas
			.authenticationManager(am);
			//.userDetailsService(userDetailsService);
	}

	@Bean
	public JwtAccessTokenConverter accessTokenConverter()  {
		JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
		accessTokenConverter.setSigningKey("fafica");
		return accessTokenConverter;
	}

	@Bean
	public TokenStore tokenStore() {
		return new JwtTokenStore(accessTokenConverter());
	}
}
