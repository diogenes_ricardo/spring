package com.fafica.web.api.config;

import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@EnableWebSecurity
@Profile("basic-security")
public class SecurityConfigBasic extends WebSecurityConfigurerAdapter {

	@Override protected void configure(AuthenticationManagerBuilder auth) throws
	  Exception { // Tipo de autenticação (memória) e usuário e senha com as regras desse usuário // para autorização
	  auth.inMemoryAuthentication().withUser("admin").password("admin").roles(
	  "ROLE"); }

	@Override protected void configure(HttpSecurity http) throws Exception { //
	  http.authorizeRequests().anyRequest().authenticated(); // qualquer requisição
	  http.authorizeRequests().antMatchers("/categorias").permitAll() //categorias está aberta 
	  .anyRequest().authenticated().and() //as outras todas terão que está autenticadas 
	  .httpBasic().and() //sob o método basic
	  .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
	  and() .csrf().disable(); }

}
