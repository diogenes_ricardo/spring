package com.fafica.web.api.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "pessoas")
public class Pessoa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Size(min = 3 , max = 20)
	private String nome;
	
	@Embedded
	private Endereco end;
	
	@NotNull
	private boolean ativo;
	
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	public Endereco getEnd() {
		return end;
	}
		
	public boolean isAtivo() {
		return ativo;
	}

	public void setEnd(Endereco end) {
		this.end = end;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
