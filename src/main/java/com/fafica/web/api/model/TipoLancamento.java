package com.fafica.web.api.model;

public enum TipoLancamento {

	RECEITA,
	DESPESA
}
