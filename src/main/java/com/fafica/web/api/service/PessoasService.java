package com.fafica.web.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.fafica.web.api.model.Pessoa;
import com.fafica.web.api.repository.PessoaRepository;

@Service
public class PessoasService {

	@Autowired
	PessoaRepository pessoaRepository;
	
	public Pessoa atualizar(Long id, Pessoa p) {
		Pessoa pSalva = buscaPessoaPorId(id);
		BeanUtils.copyProperties(p, pSalva,"id"); //copiar de p para pSalva as propriedades destas		
		return pessoaRepository.save(pSalva);
	}

	public void atualizarStatus(Long id, Boolean ativo) {
		Pessoa pSalva = buscaPessoaPorId(id);
		pSalva.setAtivo(ativo);
		pessoaRepository.save(pSalva);
	}
	
	public Pessoa buscaPessoaPorId(Long id) {
		Optional<Pessoa> pSalva = pessoaRepository.findById(id);
		if (!pSalva.isPresent()) {
			throw new EmptyResultDataAccessException(1);//pelo menos um elemento esperado
		}
		return pSalva.get();
	}
}
