package com.fafica.web.api.service;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fafica.web.api.model.Lancamento;
import com.fafica.web.api.model.Pessoa;
import com.fafica.web.api.repository.LancamentoRepository;
import com.fafica.web.api.repository.PessoaRepository;
import com.fafica.web.api.service.exception.PessoaInexisteOuInativaException;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository lancamentoRepository;

	@Autowired
	private PessoaRepository pessoaRepository;

	public Lancamento salvar(@Valid Lancamento l) {
		Pessoa p = pessoaRepository.findById(l.getPessoa().getId()).get();
		if (p == null || !p.isAtivo()) {
			throw new PessoaInexisteOuInativaException();
		}
		return lancamentoRepository.save(l);
	}

}
