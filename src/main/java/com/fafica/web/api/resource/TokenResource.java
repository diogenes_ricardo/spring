package com.fafica.web.api.resource;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fafica.web.api.config.property.FaficaAPIProperty;



@RestController
@RequestMapping("/tokens")
public class TokenResource {

	@Autowired
	private FaficaAPIProperty faficaApiProperty;

	@DeleteMapping("/revoke")
	public void revoke(HttpServletRequest request, HttpServletResponse response) {
		Cookie c = new Cookie("resfreshToken", null);
		c.setHttpOnly(true);
		c.setSecure(faficaApiProperty.getSeguranca().isEnableHttps());
		//c.setSecure(false);
		c.setPath(request.getContextPath() + "/oauth/token");
		c.setMaxAge(0);

		response.addCookie(c);
		response.setStatus(HttpStatus.NO_CONTENT.value());
	}
}
