package com.fafica.web.api.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fafica.web.api.events.RecursoCriadoEvent;
import com.fafica.web.api.model.Pessoa;
import com.fafica.web.api.repository.PessoaRepository;
import com.fafica.web.api.service.PessoasService;

@RestController
@RequestMapping("/pessoas")
public class PessoaResource {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	PessoasService pessoaService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@PostMapping	
	@PreAuthorize("hasAuthority('CADASTRAR_PESSOA') and #oauth2.hasScope('write')")
	public ResponseEntity<Pessoa> novaPessoa(@Valid @RequestBody Pessoa p, HttpServletResponse response) {
		Pessoa pSalva = pessoaRepository.save(p);		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, pSalva.getId()));		
		return ResponseEntity.status(HttpStatus.CREATED).body(pSalva);// retornando o objeto salvo na resposta
		
	}
	
	@GetMapping
	@PreAuthorize("hasAuthority('PESQUISAR_PESSOA') and #oauth2.hasScope('read')")
	public List<Pessoa> listar() {
		return pessoaRepository.findAll();
	}
	
			
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('PESQUISAR_PESSOA') and #oauth2.hasScope('read')")
	public Optional<Pessoa> listarPessoa(@PathVariable Long id) {
		return pessoaRepository.findById(id);
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('REMOVER_PESSOA') and #oauth2.hasScope('write')")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable Long id) {
		pessoaRepository.deleteById(id);
	}
	
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('CADASTRAR_PESSOA') and #oauth2.hasScope('write')")
	public ResponseEntity<Pessoa> atualizar(@PathVariable Long id, @Valid @RequestBody Pessoa p){
		Pessoa pSalva = pessoaService.atualizar(id, p);
		return ResponseEntity.ok(pSalva);
	}
	
	@PutMapping("/{id}/ativo")	
	@PreAuthorize("hasAuthority('CADASTRAR_PESSOA') and #oauth2.hasScope('write')")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizarStatus(@PathVariable Long id, @RequestBody Boolean ativo){
		pessoaService.atualizarStatus(id,ativo);		
	}
	
	
}
