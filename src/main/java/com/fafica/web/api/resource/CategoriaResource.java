package com.fafica.web.api.resource;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fafica.web.api.model.Categoria;
import com.fafica.web.api.repository.CategoriaRepository;

@RestController
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	private CategoriaRepository categoriaRepository;
	
//	@CrossOrigin (origins = {"http://localhost:8081"})
	@GetMapping
	@PreAuthorize("hasAuthority('PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public List<Categoria> listar() {
		return categoriaRepository.findAll();
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('CADASTRAR_CATEGORIA') and #oauth2.hasScope('write')")
	public ResponseEntity<Categoria> novaCategoria(@Valid @RequestBody Categoria p,HttpServletResponse response) {
		Categoria pSalva = categoriaRepository.save(p);
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}")
			.buildAndExpand(pSalva.getId()).toUri(); //construindo a URL de consulta do obj salvo
//		response.setHeader("Location", uri.toASCIIString());
		
		return ResponseEntity.created(uri).body(pSalva);	 // retornando o objeto salvo na resposta
		
	}
		
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('PESQUISAR_CATEGORIA') and #oauth2.hasScope('read')")
	public Categoria listarCategoria(@PathVariable Long id) {		
		return categoriaRepository.findById(id).get();
	}
}
