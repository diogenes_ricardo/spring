package com.fafica.web.api.resource;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fafica.web.api.events.RecursoCriadoEvent;
import com.fafica.web.api.exceptionhandler.Aula02ExceptionHandler.Erro;
import com.fafica.web.api.model.Lancamento;
import com.fafica.web.api.repository.LancamentoRepository;
import com.fafica.web.api.repository.filter.LancamentoFilter;
import com.fafica.web.api.repository.projection.ResumoLancamento;
import com.fafica.web.api.service.LancamentoService;
import com.fafica.web.api.service.exception.PessoaInexisteOuInativaException;

@RestController
@RequestMapping("/lancamentos")
@CrossOrigin
public class LancamentoResource {

	@Autowired
	private LancamentoRepository lancamentoRepository;
	
	@Autowired
	LancamentoService lancamentoService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private MessageSource msgsource;
	
	/*@GetMapping
	public List<Lancamento> listar() {
		return lancamentoRepository.findAll();
	}*/
	@GetMapping
	@PreAuthorize("hasAuthority('PESQUISAR_LANCAMENTO') and #oauth2.hasScope('read')")
	public Page<Lancamento> pesquisar(LancamentoFilter lf, Pageable p) {
		return lancamentoRepository.filtrar(lf, p);
	}
	
	@GetMapping(params= "resumo")
	@PreAuthorize("hasAuthority('PESQUISAR_LANCAMENTO') and #oauth2.hasScope('read')")
	public Page<ResumoLancamento> resumir(LancamentoFilter lf, Pageable p) {
		return lancamentoRepository.resumir(lf, p);
	}
	
			
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('PESQUISAR_LANCAMENTO') and #oauth2.hasScope('read')")
	public Lancamento listarLancamento(@PathVariable Long id) {
		return lancamentoRepository.findById(id).get();
	}
	
	@PostMapping	
	@PreAuthorize("hasAuthority('CADASTAR_LANCAMENTO') and #oauth2.hasScope('write')")
	public ResponseEntity<Lancamento> novoLancamento(@Valid @RequestBody Lancamento l, HttpServletResponse response) {
		Lancamento lSalvo = lancamentoRepository.save(l);
		lancamentoService.salvar(l);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, lSalvo.getId()));		
		return ResponseEntity.status(HttpStatus.CREATED).body(lSalvo);// retornando o objeto salvo na resposta
		
	}
	
	@DeleteMapping("/{id}")
	@PreAuthorize("hasAuthority('REMOVER_LANCAMENTO') and #oauth2.hasScope('write')")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deletar(@PathVariable Long id) {
		lancamentoRepository.deleteById(id);
	} 
	
	@ExceptionHandler({PessoaInexisteOuInativaException.class})
	public ResponseEntity<Object> handlePessoaInexisteOuInativaException(PessoaInexisteOuInativaException ex) {
		String msgUsuario = msgsource.getMessage("pessoa.inexistente.ou.inativa", null, LocaleContextHolder.getLocale());
		String msgDev = ex.toString();
		List<Erro> listaErros = Arrays.asList(new Erro(msgUsuario,msgDev));
		return ResponseEntity.badRequest().body(listaErros);
		
	}
	
	
	
}
