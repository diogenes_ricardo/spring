package com.fafica.web.api.exceptionhandler;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class Aula02ExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource msgsource;

	// Erro de propriedades desconhecidas
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		String msgUsuario = msgsource.getMessage("objetojsoninvalido", null, LocaleContextHolder.getLocale());
		String msgDev = ex.getCause() != null ? ex.getCause().toString() : ex.toString();

		return handleExceptionInternal(ex, new Erro(msgUsuario, msgDev), headers, status, request);
	}

	// Erro de argumentos inválidos ie. null
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<Erro> listaErros = criarListadeErros(ex.getBindingResult());
		return handleExceptionInternal(ex, listaErros, headers, status, request);
	}
	
	@ExceptionHandler({EmptyResultDataAccessException.class})
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest wb) {
		String msgUsuario = msgsource.getMessage("recurso.naoencontrado", null, LocaleContextHolder.getLocale());
		String msgDev = ex.getMessage();
		
		return handleExceptionInternal(ex, new Erro(msgUsuario, msgDev), new HttpHeaders() , HttpStatus.NOT_FOUND, wb);
	}

	@ExceptionHandler({DataIntegrityViolationException.class})
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest wb) {
		String msgUsuario = msgsource.getMessage("recurso.operacao-nao-permitida", null, LocaleContextHolder.getLocale());
		String msgDev = ExceptionUtils.getRootCauseMessage(ex);
		
		return handleExceptionInternal(ex, new Erro(msgUsuario, msgDev), new HttpHeaders() , HttpStatus.BAD_REQUEST, wb);
	}
	
	private List<Erro> criarListadeErros(BindingResult br) { //BindingResult resultados da validação
		List<Erro> erros = new ArrayList<>();

		for (FieldError	fe : br.getFieldErrors()) { //getFieldsErros traz uma lista de erros 
			String msgUsuario = msgsource.getMessage(fe, LocaleContextHolder.getLocale());
			String msgDev = fe.toString();
			erros.add(new Erro(msgUsuario,msgDev));	
		}
		return erros;
	}

	public static class Erro {
		private String msgUsuario;
		private String msgDev;

		public Erro(String msgUsuario, String msgDev) {
			this.msgUsuario = msgUsuario;
			this.msgDev = msgDev;
		}

		public String getMsgUsuario() {
			return msgUsuario;
		}

		public String getMsgDev() {
			return msgDev;
		}
	}

}
