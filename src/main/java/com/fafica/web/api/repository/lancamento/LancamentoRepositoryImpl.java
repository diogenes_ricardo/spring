package com.fafica.web.api.repository.lancamento;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.fafica.web.api.model.Categoria;
import com.fafica.web.api.model.Categoria_;
import com.fafica.web.api.model.Lancamento;
import com.fafica.web.api.model.Lancamento_;
import com.fafica.web.api.model.Pessoa_;
import com.fafica.web.api.repository.filter.LancamentoFilter;
import com.fafica.web.api.repository.projection.ResumoLancamento;

public class LancamentoRepositoryImpl implements LancamentoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Lancamento> filtrar(LancamentoFilter lf, Pageable p) {
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery<Lancamento> cq = cb.createQuery(Lancamento.class);
		Root<Lancamento> root = cq.from(Lancamento.class);

		// aqui são os parâmetros do filtro
		Predicate[] predicates = criarRestricoes(lf, cb, root);
		cq.where(predicates);

		TypedQuery<Lancamento> query = manager.createQuery(cq);

		adicionarRestricoesDePaginacao(query, p);

		return new PageImpl<>(query.getResultList(), p, total(lf));
	}

	@Override
	public Page<ResumoLancamento> resumir(LancamentoFilter lf, Pageable p) {
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoLancamento> cq = cb.createQuery(ResumoLancamento.class);
		Root<Lancamento> root = cq.from(Lancamento.class);//de onde buscar

		cq.select(cb.construct(ResumoLancamento.class
				,root.get("id"),root.get("descricao")
				,root.get("dataVencimento"),root.get("dataPagamento")
				,root.get("valor"),root.get("tipo")
				,root.get("categoria").get("descricao")
				,root.get("pessoa").get("nome")));	

		Predicate[] predicates = criarRestricoes(lf, cb, root);
		cq.where(predicates);
		
		TypedQuery<ResumoLancamento> query = manager.createQuery(cq);

		adicionarRestricoesDePaginacao(query, p);

		return new PageImpl<>(query.getResultList(), p, total(lf));
	}
	
	private Predicate[] criarRestricoes(LancamentoFilter lf, CriteriaBuilder cb, Root<Lancamento> root) {
		
		List<Predicate> predicates = new ArrayList<Predicate>();
		if (!StringUtils.isEmpty(lf.getDescricao())) {
			predicates.add(
					cb.like(cb.lower(root.get(Lancamento_.descricao)), "%" + lf.getDescricao().toLowerCase() + "%"));
		}

		if (lf.getDataVencimentoDe() != null) {
			predicates.add(cb.greaterThanOrEqualTo(root.get(Lancamento_.dataVencimento), lf.getDataVencimentoDe()));
		}

		if (lf.getDataVencimentoAte() != null) {
			predicates.add(cb.lessThanOrEqualTo(root.get(Lancamento_.dataVencimento), lf.getDataVencimentoAte()));
		}

		return predicates.toArray(new Predicate[predicates.size()]);
	}

	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable p) {
		
		int paginaAtual = p.getPageNumber();
		int registrosPorPagina = p.getPageSize();

		int primeiroRegistroExibidoNaPagina = paginaAtual * registrosPorPagina; // pagina 3 * 3 registro = 9, logo elemento 9
																		// será o primeiro da lista
		
		query.setFirstResult(primeiroRegistroExibidoNaPagina);
		query.setMaxResults(registrosPorPagina);
	}

	private Long total(LancamentoFilter lf) {
		
		CriteriaBuilder cb = manager.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Lancamento> root = cq.from(Lancamento.class);
		
		Predicate[] predicates = criarRestricoes(lf, cb, root);		
		cq.where(predicates);
		
		cq.select(cb.count(root));
		 
		return manager.createQuery(cq).getSingleResult();
		
	}


}
