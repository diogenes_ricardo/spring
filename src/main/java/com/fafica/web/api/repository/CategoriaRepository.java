package com.fafica.web.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fafica.web.api.model.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Long> {

}
