package com.fafica.web.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fafica.web.api.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

}
