package com.fafica.web.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.fafica.web.api.model.Lancamento;
import com.fafica.web.api.repository.lancamento.LancamentoRepositoryQuery;

public interface LancamentoRepository extends JpaRepository<Lancamento, Long>, LancamentoRepositoryQuery {

}
