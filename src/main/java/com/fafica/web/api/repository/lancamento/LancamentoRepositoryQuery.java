package com.fafica.web.api.repository.lancamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.fafica.web.api.model.Lancamento;
import com.fafica.web.api.repository.filter.LancamentoFilter;
import com.fafica.web.api.repository.projection.ResumoLancamento;

public interface LancamentoRepositoryQuery {	
	public Page<Lancamento> filtrar(LancamentoFilter lf, Pageable p);
	public Page<ResumoLancamento> resumir(LancamentoFilter lf, Pageable p);
	

}
